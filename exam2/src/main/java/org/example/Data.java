package org.example;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class Data {

    private String jamMasuk;
    private String kendaraan;
    private int idParkir;
    private int hasil;

    public Data (String jamMasuk, String kendaraan){
        this.jamMasuk=jamMasuk;
        this.kendaraan=kendaraan;

        DateTimeFormatter newParsers = DateTimeFormatter.ofPattern("yyyy-MM-dd, h:mma");
        LocalDateTime newMasuk = LocalDateTime.parse(jamMasuk, newParsers);
        DateTimeFormatter newParse2 = DateTimeFormatter.ofPattern("H");
        String newMasuk2 = newMasuk.format(newParse2);
        int newIn = Integer.parseInt(newMasuk2);


        LocalDateTime keluar = LocalDateTime.now();
        DateTimeFormatter newParse = DateTimeFormatter.ofPattern("H");
        String newKeluar = keluar.format(newParse);
        int newOut = Integer.parseInt(newKeluar);

        if(kendaraan == "Motor") {
            long hasil2 = ChronoUnit.HOURS.between(newMasuk,keluar);
            hasil = (int) ((hasil2-1) * 1000 + 3000);
        }
        else if(kendaraan == "Mobil"){
            long hasil3 = ChronoUnit.HOURS.between(newMasuk,keluar);
            hasil = (int) ((hasil3-1) * 2000 + 5000);
        }



    }

    public int getString(){
        LocalDateTime keluar = LocalDateTime.now();
        DateTimeFormatter newParser = DateTimeFormatter.ofPattern("yyyy-MM-dd, h:mma");
        String newKeluar2 = keluar.format(newParser);
        System.out.println("Anda masuk jam: " + jamMasuk);
        System.out.println("Anda keluar jam: " + newKeluar2);

        return hasil;
    }


//    public void show(){
//        LocalDateTime now2 = LocalDateTime.now();
//        DateTimeFormatter newParse2 = DateTimeFormatter.ofPattern("yyyy-MM-dd, h:mma");
//        String formatted2 = now2.format(newParse2);
//        System.out.println(formatted2);
//
//        LocalTime now = LocalTime.now();
//        DateTimeFormatter newParse = DateTimeFormatter.ofPattern("H");
//        String formatted = now.format(newParse);
//        System.out.println(formatted);
//    }
    }
